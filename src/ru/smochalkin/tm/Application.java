package ru.smochalkin.tm;

import ru.smochalkin.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        displayWelcome();
        parseArgs(args);
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (TerminalConst.HELP.equals(arg)) showHelp();
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
    }

    public static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + " - Display developer info.");
        System.out.println(TerminalConst.VERSION + " - Display version.");
        System.out.println(TerminalConst.HELP + " - Display list of terminal commands.");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Sergey Mochalkin");
        System.out.println("smochalkin@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

}
